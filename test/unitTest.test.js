const assert = require('assert');
var expect = require('chai').expect;
const app = require('../app')
var request = require('supertest');
var agent = request.agent(app);
const fs = require('fs');


describe('Unit testing the / route', function() {
    it('should show the home page with OK status', function() {
      return request(app)
        .get('/')
        .then(function(response) {
            assert(response.status, 200);
        })
    });
});

describe('Unit testing the /login route', function () {
    it('should login success', function(done) {
        agent
            .post('/login')
            .type('form')
            .send({ txtEmail: 'minhtintang@gmail.com' })
            .send({ txtPass: '123' })
            .expect(302)
            .expect('Location', '/')
            .expect('set-cookie', /connect.sid/)
            .end(function(err, res) {
                if (err) return done(err);
                return done();
            });
    });

    it('should login fail', function(done) {
        agent
            .post('/login')
            .type('form')
            .send({ txtEmail: 'minhtintang@gmail.com' })
            .send({ txtPass: 'mmm' })
            .expect(302)
            .expect('Location', '/login')
            .end(function(err, res) {
                if (err) return done(err);
                return done();
            });
    });
});

describe('Unit testing page not found', function() {
    it('should return 404 status', function() {
      return request(app)
        .get('/xxxxxxxx')
        .then(function(response) {
            assert(response.status, 404);
        })
        .then(() => {
            setTimeout((function() {
                return process.exit();
            }), 5000);            
        })
    });
});



describe('Unit testing the /registration route', function () {
  // it('should registration success', function(done) {
  //     agent
  //         .post('/registration')
  //         .type('form')
  //         .send({ fullname: 'Nguyen Son Hao' })
  //         .send({ email: 'Haovippro@gmail.com' })
  //         .send({ password: '24120' })
  //         .send({ confirm: '24120' })
  //         .send({ image: "uploads\\Unti1tled.png" })
  //         .expect(302)
  //         .expect('Location', '/login')
  //         .end(function(err, res) {
  //             if (err) return done(err);
  //             return done(); 
  //          });
  // });

  it('should registration fail (email existed)', function (done) {
    agent
      .post('/registration')
      .type('form')
      .send({ fullname: 'Tin Minh Tang' })
      .send({ email: 'minhtintang@gmail.com' })
      .send({ password: '123' })
      .send({ confirm: '123' })
      .send({ image: '123' })
      .expect(302)
      .expect('Location', '/registration')
      .end(function (err, res) {
        if (err) return done(err);
        return done();
      });
  });

  it('should registration fail (password not match)', function (done) {
    agent
      .post('/registration')
      .type('form')
      .send({ fullname: 'Tin Minh Tang' })
      .send({ email: 'abc234@gmail.com' })
      .send({ password: 'sdfias3' })
      .send({ confirm: '123' })
      .send({ image: '123' })
      .expect(302)
      .expect('Location', '/registration')
      .end(function (err, res) {
        if (err) return done(err);
        return done();
      });
  });

  // it('comment', function(done) {
  //     agent
  //         .post('/login')
  //         .type('form')
  //         .send({ txtEmail: 'minhtintang@gmail.com' })
  //         .send({ txtPass: '123' })

  //         .expect(302)
  //         .expect('Location', '/registration')
  //         .end(function(err, res) {
  //             if (err) return done(err);
  //             return done();
  //         });
  // });
});

// describe('WebSocket test', function () {
//   it('should signup a user',
//     async () => {
//       agent
//         .post('/registration')
//         .set('Content-Type', 'application/x-www-form-urlencoded')
//         .field('fullname', 'franklin')
//         .field('email', 'Isaiah@gmail.com')
//         .field('password', '123345756')
//         .field('confirm', '123345756')
//         .attach('image',
//           fs.readFileSync('C:\\Users\\hiend\\OneDrive\\Desktop\\image\\Mac\\Product15.4.png'),
//           'preview.png')
//         .expect('Location', '/registration')
//     },
//   );
// });



//         describe('WebSocket test', function() {
//             //assert.equal(response.result, null, 'Successful Authentification');
//         });

//     });
// });
// it('Main page content', function(done) {
//     request('http://localhost:3000/login' , function(error, response, body) {
//         expect(body).to.equal('Hello World');
//         done();
//     });
// })
